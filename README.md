# jugendhackt-iso.sh

Bootstraps a persistent Ubuntu-based live system on a USB stick.

```shell
jugendhackt-iso.sh <usb-device> <iso-file>
```

License: [EUPL-1.2](LICENSE)

Author: The one with the braid <the-one@with-the-braid.cf>
